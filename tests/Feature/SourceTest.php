<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery\Exception;
use Tests\TestCase;

class SourceTest extends TestCase
{
    /**
     * Check if service working
     *
     * @return void
     */
    public function testApplication()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * Check if csv API working
     *
     * @return void
     */
    public function testCsvApi()
    {
        $response = $this->get('/dbs/foo/tables/source/csv/');
        $response->assertStatus(200);
    }

    /**
     * Check if json API working
     *
     * @return void
     */
    public function testJsonApi()
    {
        $response = $this->get('/dbs/foo/tables/source/json/');
        $response->assertStatus(200);
    }
}
