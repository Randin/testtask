<?php

use App\Models\Source;
use Illuminate\Database\Seeder;

class SourceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $numOfRows = 1000000;
        $chunkSize = 20000;
        $chunks = $numOfRows / $chunkSize;

        for ($i = 0; $i < $chunks; $i++){
            factory(Source::class, $chunkSize)->create();
        }
    }
}
