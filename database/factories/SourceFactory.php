<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Source;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Source::class, function () {
    static $a = 0;
    $a++;

    return [
        'b' => $a % 3,
        'c' => $a % 5
    ];
});
