<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SourceJsonRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'page' => 'numeric',
            'page_size' => 'numeric'
        ];
    }
}
