<?php

namespace App\Http\Controllers;

use App\Http\Requests\SourceJsonRequest;
use App\Models\Source;
use App\Services\CsvFileService;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Class SourceController
 * @package App\Http\Controllers
 */
class SourceController extends Controller
{
    /**
     * Default page size for json api
     */
    const DEFAULT_PAGE_SIZE = 10;
    /**
     * @var CsvFileService
     */
    private $fileService;

    /**
     * SourceController constructor.
     */
    public function __construct()
    {
        $this->fileService = new CsvFileService(Source::class);
    }

    /**
     * Returns chunked csv file with Source data
     *
     * @return StreamedResponse
     */
    public function indexCsv() : StreamedResponse
    {
        $response = $this->fileService;

        return $response->handle();
    }

    /**
     * Returns paged json file with Source data
     *
     * @param SourceJsonRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function indexJson(SourceJsonRequest $request) : JsonResponse
    {
        $sources = Source::paginate($request->page_size ?? self::DEFAULT_PAGE_SIZE);
        return response()->json($sources, Response::HTTP_OK);
    }
}
