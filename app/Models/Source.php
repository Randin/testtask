<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Contracts\CsvHandlable;

/**
 * Class Source
 * @package App\Models
 */
class Source extends Model implements CsvHandlable
{
    /**
     * Primary key
     *
     * @var string
     */
    protected $primaryKey = 'a';


    /**
     * Table for model
     *
     * @var string
     */
    protected $table = 'source';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
    * Fillable fields
    *
    * @var array
    */
    protected $fillable = [
        'a',
        'b',
        'c'
    ];

    /**
     * CSV fields
     *
     * @var array
     */
    private static $csvAttributes = [
        'a',
        'b',
        'c'
    ];

    /**
     * Returns csv attributes
     *
     * @return array
     */
    public static function getCsvAttributes() : array
    {
        return self::$csvAttributes;
    }
}
