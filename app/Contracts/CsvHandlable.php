<?php
namespace App\Contracts;

/**
 * Interface CsvHandlable
 * @package App\Contracts
 */
interface CsvHandlable
{
    /**
     * Returns csv attributes
     */
    public static function getCsvAttributes();
}
