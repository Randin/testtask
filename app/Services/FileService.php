<?php
namespace App\Services;

/**
 * Interface FileService
 * @package App\Services
 */
interface FileService
{
    /**
     * Handles the file
     *
     * @return mixed
     */
    public function handle();
}
