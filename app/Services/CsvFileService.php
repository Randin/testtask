<?php
namespace App\Services;

use App\Contracts\CsvHandlable;
use Illuminate\Support\Facades\App;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Class CsvFileService
 * @package App\Services
 */
class CsvFileService implements FileService
{
    /**
     * Size of the chunk
     */
    const CHUNK_SIZE = 100;

    /**
     * @var
     */
    private $fileHandle;

    /**
     * @var string
     */
    private $model;

    /**
     * CsvFileService constructor.
     * @param string $model
     * @throws \Exception
     */
    public function __construct(string $model)
    {
        if(in_array(CsvHandlable::class, class_implements($model))) {
            $this->model = $model;
        }else{
            throw new \Exception('Class ['.$model.'] does not implement CsvHandlable interface');
        }
    }

    /**
     * Handles the file
     *
     * @return StreamedResponse
     */
    public function handle() : StreamedResponse
    {
        $response = new StreamedResponse(function(){
            $this->openFile();
            $this->addBomUtf8();
            $this->addHeadersToCsv();
            $this->processData();
            $this->closeFile();
        }, Response::HTTP_OK, [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="export.csv"'
        ]);

        return $response;
    }

    /**
     * Opens file
     */
    private function openFile()
    {
        $this->fileHandle = fopen('php://output', 'w');
    }

    /**
     * Adds header to the csv according to the model settings
     */
    private function addHeadersToCsv()
    {
        $this->putRowInCsv(
            call_user_func($this->model . '::getCsvAttributes')
        );
    }

    /**
     * Adds new row to csv
     *
     * @param array $data
     */
    private function putRowInCsv(array $data){
        fputcsv($this->fileHandle, $data);
    }

    /**
     * Adds BOM
     */
    private function addBomUTf8(){
        fwrite($this->fileHandle, $bom = (chr(0xEF).chr(0xBB).chr(0xBF)));
    }

    /**
     * Prepares models data for csv
     */
    private function processData()
    {
        call_user_func_array($this->model . '::orderBy', [
                App::make($this->model)->getKeyName()
            ]
        )->chunk(self::CHUNK_SIZE, function($sources) {
            foreach ($sources as $source) {
                $this->addLine($source);
            }
        });
    }

    /**
     * Adds new line to csv
     *
     * @param $line
     */
    private function addLine($line)
    {
        $record = [];

        foreach (call_user_func($this->model . '::getCsvAttributes') as $attribute)
        {
            $record[] = $line->{$attribute};
        }

        $this->putRowInCsv($record);
    }

    /**
     * Closes file
     */
    private function closeFile()
    {
        fclose($this->fileHandle);
    }
}
