## Installation

- docker-compose up -d
- docker exec -ti <service> bash
- cp .env.example .env
- php artisan key:generate
- composer install
- php artisan migrate

##Testing
- php artisan db:seed
- php artisan test

## Assumptions for further development

- More tests (lack of time)
- Add error checking
